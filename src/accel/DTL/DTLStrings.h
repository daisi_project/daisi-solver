#ifndef DTLStrings_H
#define DTLStrings_H
std::string DTLSolverGenerateElectrodes  = "Generate electrodes for flow";
std::string DTLSolverCalculateAcceptance = "Calculate acceptance";
std::string DTLSolverSimulateEnvelopes   = "Simulate dynamics envelopes";
std::string DTLSolverSimulateBeam        = "Simulate dynamics beam";
std::string DTLSolverExport              = "Export to DAISI simulations module";
std::string DTLOptimization              = "Optimization";
#endif